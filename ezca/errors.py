class EzcaError(Exception):
    pass

class EzcaConnectError(Exception):
    pass

class LIGOFilterError(Exception):
    pass
